object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 451
  ClientWidth = 598
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 337
    Top = 38
    Width = 138
    Height = 45
    Caption = 'Interns'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -37
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Button1: TButton
    Left = 42
    Top = 8
    Width = 167
    Height = 41
    Caption = 'Connect'
    TabOrder = 0
    OnClick = Button1Click
  end
  object DBGrid1: TDBGrid
    Left = 240
    Top = 94
    Width = 329
    Height = 298
    DataSource = DataSource1
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object Button2: TButton
    Left = 40
    Top = 55
    Width = 169
    Height = 33
    Caption = 'load data'
    Enabled = False
    TabOrder = 2
    OnClick = Button2Click
  end
  object Panel1: TPanel
    Left = 40
    Top = 94
    Width = 169
    Height = 329
    TabOrder = 3
    object Edit1: TEdit
      Left = 16
      Top = 72
      Width = 121
      Height = 21
      TabOrder = 0
      TextHint = 'First Name'
    end
    object Edit2: TEdit
      Left = 16
      Top = 104
      Width = 121
      Height = 21
      TabOrder = 1
      TextHint = 'Last Name'
    end
    object Button3: TButton
      Left = 16
      Top = 139
      Width = 75
      Height = 25
      Caption = 'Add'
      TabOrder = 2
      OnClick = Button3Click
    end
    object Panel2: TPanel
      Left = -8
      Top = 0
      Width = 185
      Height = 41
      Caption = 'New Student'
      TabOrder = 3
    end
  end
  object Button4: TButton
    Left = 400
    Top = 398
    Width = 75
    Height = 25
    Caption = 'Update'
    TabOrder = 4
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 494
    Top = 398
    Width = 75
    Height = 25
    Caption = 'Delete'
    TabOrder = 5
    OnClick = Button5Click
  end
  object FDConnection1: TFDConnection
    Params.Strings = (
      'DriverID=PG')
    Left = 312
    Top = 8
  end
  object FDQuery1: TFDQuery
    Connection = FDConnection1
    Left = 416
    Top = 176
  end
  object FDPhysPgDriverLink1: TFDPhysPgDriverLink
    Left = 504
    Top = 8
  end
  object DataSource1: TDataSource
    DataSet = FDQuery1
    Left = 496
    Top = 176
  end
  object FDManager1: TFDManager
    FormatOptions.AssignedValues = [fvMapRules]
    FormatOptions.OwnMapRules = True
    FormatOptions.MapRules = <>
    Active = True
    Left = 400
    Top = 8
  end
  object FDQuery2: TFDQuery
    Connection = FDConnection1
    Left = 120
    Top = 312
  end
  object FDQuery3: TFDQuery
    Connection = FDConnection1
    Left = 536
    Top = 344
  end
  object PopupActionBar1: TPopupActionBar
    Left = 312
    Top = 248
  end
end

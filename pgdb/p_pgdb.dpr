program p_pgdb;

uses
  Vcl.Forms,
  pgdb in 'pgdb.pas' {Form1},
  delete_window in 'delete_window.pas' {Form2},
  update_window in 'update_window.pas' {Form3};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TForm2, Form2);
  Application.CreateForm(TForm3, Form3);
  Application.Run;
end.
